import React from 'react'
import './achivement.css'
import DashboardIcon from '@material-ui/icons/Dashboard';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import CreateIcon from '@material-ui/icons/Create';
function Achivement() {
    return (
        <div className='achivement'>
            <div className='achive'>
              {/* <VerifiedIcon/> */}
              < DashboardIcon className='avatar'/>
              <h4>3</h4>
              <p>Achivements</p>
            </div>
            <div className='coins'>
                <AccountBalanceWalletIcon className='avatar'/>
                <h4>300</h4>
                <p>Coins</p>
            </div> 
            <div className='blogs'>
                <CreateIcon className='avatar'/>
                <h4>3</h4>
                <p>Blogs</p>
            </div>
        </div>
    )
}

export default Achivement
