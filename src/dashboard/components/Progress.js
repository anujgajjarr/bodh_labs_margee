import React from 'react'
import './progress.css'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
function Progress() {
    return (
        <div className='progress'>
            <h4>Profile Progress</h4>
            <p className='i1'><CheckCircleIcon className='i2'/>----
            <RadioButtonUncheckedIcon className='i2'/>----
            <RadioButtonUncheckedIcon className='i2'/>----
            <RadioButtonUncheckedIcon className='i2'/></p>
            <div className='check'>
            <p > Basic Informations</p>
            <p > Address detail</p>
            <p > Social informations</p>
            <p > other details</p>
            </div>
        </div>
    )
} 

export default Progress
