import React, { useState } from 'react'
import "./Dashboard.css"
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import AssignmentIcon from '@material-ui/icons/Assignment';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import ReportIcon from '@material-ui/icons/Report';
import AssessmentIcon from '@material-ui/icons/Assessment';
import HelpIcon from '@material-ui/icons/Help';
import SearchIcon from '@material-ui/icons/Search';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Profile from './Profile'
import Graph from './Graph'
import Progress from './Progress';
import Achivement from './Achivement'
import CloseIcon from '@material-ui/icons/Close'
import MenuIcon from '@material-ui/icons/Menu';
import styled from 'styled-components'
function Dashboard() {
  const [sign,open]=useState(false)
    return (
        <div className='dashboard'>
           <div className='navbar'>
              <img src='https://st2.depositphotos.com/1104517/11965/v/600/depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg' alt=''/> 
             <Wrap><p>Harsh Patel</p><ArrowDropDownIcon  onClick={()=>open(true)}/></Wrap>
             <Burgeropen show={sign}>
               <CloseWrap onClick={()=>open(false)}/>
               <button>Signout</button>
             </Burgeropen>
              <SearchIcon className='button'/> <input type="text" placeholder='QuickSearch'/>
             <NotificationsIcon className='button'/>
           </div>
         <div className='content'>
           <div className='bar'>
             <div className='options'>
                <p className='dash'><DashboardIcon className='mu'/> Dashboard</p>
                <p className='boards-icons'><PeopleAltIcon  className='mu'/> People</p>
                <p className='boards-icons'><AssessmentIcon  className='mu'/> Project</p>
                <p className='boards-icons'><InsertInvitationIcon  className='mu'/>Calendar</p> 
                <p className='boards-icons'> <ReportIcon  className='mu'/> Reports</p>
                <p className='boards-icons'><AssignmentIcon  className='mu'/> Administration</p>
                <p className='boards-icons'> <HelpIcon  className='mu'/> Help</p>
                </div>
                <h4>Bodh labs</h4>
           </div>
           <div className='middle'>
              <div className='profile'>
                <Profile/>
              </div>
              <div className='graph'>
                <Graph/>
              </div>
              </div> 
             <div className='rightside'>
                <div className='achivement'>
                  <Achivement/>
                </div>     
                <div className='progress'>
                  <Progress/>
                </div>
                </div>  
          

        </div>
       
        </div>
    )
}

export default Dashboard

const Burgeropen=styled.div`
position:fixed;
top:50px;
bottom:0;
left:100px;
background:white;
width:200px;
z-index:16;
list-style:none;
padding:20px
dispaly:flex;
flex-direction:column;
text-align:start; 
transform:${props=>props.show?'translateY(0)':'translateY(100%)'};
button{
  background-color:black;
  color:white;
  border:none;
  outline:none;
}
`
const CloseWrap=styled(CloseIcon)`
cursor:pointer;
`
const Wrap=styled.div`
margin:5px 20px 0px 2px;
display:flex;
align-item:center;
`