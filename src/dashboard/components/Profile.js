import React from 'react'
import './Profile.css'
import LocationOnIcon from '@material-ui/icons/LocationOn';
function Profile() {
    return (
        < div className='main'>
        <div className='pro'>
            <div className='image'>
            <img src='https://st2.depositphotos.com/1104517/11965/v/600/depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg' alt='img' placeholder='img'/>
            </div>
            <div className='details'>
                <h4>Harsh Patel</h4>
                <p><LocationOnIcon/> Ahmedabad,India</p>
                <div className='follow'>
                  <div className='fol'>
                  <p>Followers</p>
                  <h4>4k</h4>
                  </div>
                  <p className='line'>--------</p>
                  <div className='follw'>
                 <p>Following</p>
                 <h4>2k</h4> 
                 </div>
                 </div>
            </div>
            <button>Edit Profile</button>
        </div> 
        <div className='opts'>
            <p>Basic Details</p>
            <p>Social</p>
            <p>Blogs</p>
            <p>Progress</p>
        </div>
        </div>
    )
}

export default Profile
