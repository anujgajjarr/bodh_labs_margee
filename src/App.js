
import './App.css';
import Login from './login/Login';
import Otp2 from './otp/Otp2'
import App3 from './blog/App3';
import App4 from './dashboard/App4' 
import { BrowserRouter as Router,Switch,Route} from "react-router-dom";
function App() {
  return (
    <div className="App">
      {/* Routes of all the pages */}
      <Router>
        <Switch>
          <Route exact path='/'><Login/></Route>
          <Route path='/otp'><Otp2/></Route>
          <Route path='/blog'><App3/></Route>
          <Route path='/dashboard'><App4/></Route>
          </Switch> 
      </Router>
     
    </div>
  );
}

export default App;
