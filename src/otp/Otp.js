import React, { Component } from 'react'
import { Box, Grid} from '@material-ui/core'
import image from './image/Rectangle 4.png'
import Button from '@mui/material/Button';
import OtpInput from "react-otp-input";
import FormControl from '@mui/material/FormControl';
import './Otp.css'
import {Link} from 'react-router-dom'
export default class Otp extends Component {
    state = { otp: "" , disabled : true};

  handleChange = (otp) => {
      this.setState({ otp });
      if(otp.length === 4)  {
          this.setState ({
              disabled : false
          })
      }
      else {
          this.setState ({
              disabled : true
          })
      }
    }

    render() {
        return (
            <div>
                <Grid container spacing={2}>
                <Grid  item md={6}>
                    <Box className='image'>
                        <img src={image} alt="" />
                    </Box> 
                </Grid>
                
                <Grid item md={6} xs={12}>
                    <Box className='setvalue' component="form">
                        
                        <FormControl>
                            <Grid container justify="center" alignItems="center" >
                                <Grid item xs={12} justify="center">
                                    <label className='titel'>Bodh Labs</label>
                                    <label className='sub-titel'>OTP VERIFICATAION</label>
                                    <p className='number'>OTP sent to -<b> +91 12345 67890</b></p>
                                </Grid>

                                <Grid item xs={12} container justify="center" alignItems="center" direction="column">
                                    <Grid item spacing={3} justify="center">
                                      <OtpInput className='otp-input'
                                      value={this.state.otp}
                                      onChange={this.handleChange}
                                        separator={
                                          <span>
                                            <strong>.</strong>
                                          </span>
                                        }
                                        inputStyle={{
                                            
                                          width: "3rem",
                                          height: "3rem",
                                          margin: "0 1rem",
                                          fontSize: "2rem",
                                          borderRadius: 4,
                                          border: "1px solid rgba(0,0,0,0.3)"
                                        }}
                                      />
                                    </Grid>
                                </Grid>
                                <p class="otp_item2_p3">Please don't share OTP with anyone.</p>

                                <Grid item xs={12}>
                                 <Link to='/blog'><button  id="button" disabled={this.state.disabled}>verify</button></Link>
                                </Grid>

                                <p class="otp_item2_p4">Don't Receive OTP? <a href="/">Resend OTP </a></p>
                            </Grid>
                        </FormControl>
                    </Box>
                </Grid>
            </Grid>
            </div>
        )
    }
}

