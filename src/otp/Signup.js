import { Box, Grid} from '@material-ui/core'
import React,{useState} from 'react'
import TextField from '@mui/material/TextField';

import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';

import male from './image/Male.png'
import female from './image/Female.png'
import './Singup.css'



const Signup = () => {
    const [FirstName, setFirstName] = useState('');
    const [LastName, setLastName] = useState('');
    const [Email, setEmail] = useState('');
    const [Date, setDate] = React.useState(null);
    const [Gender, setGender] = useState('male')
    
    
    let t1 =`${Gender}`
    return (
        <div>
            <Grid container >
                <Grid item md={6} xs={12}>
                    <Box className='image'>
                        {t1==='female' ? <img src={female} className='i1' alt="male" /> : <img src={male} className='i1' alt="male" />  }
                    </Box> 
                </Grid>

                <Grid item md={6} xs={12}>
                    <Box className='setvalue' component="form">
                        
                        <FormControl>
                            <Grid container >
                                <Grid item xs={12}>
                                    <label className='titel'>Bodh Labs</label>
                                </Grid>

                                <Grid item xs={12} >
                                    <label className='sub-titel'>CREATE PROFILE</label>
                                </Grid>

                                <Grid item xs={12} md={6} >
                                    <label id='f-name' className='entry'>First Name</label><br />
                                    <TextField  style={{width:'95%'}} onChange={(e)=>setFirstName(e.target.value)} placeholder="Enter First Name" id="f-input" className='input-filed' variant="filled" required />
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    <label id='l-name' className='entry'>Last Name</label><br />
                                    <TextField style={{width:'95%'}} onChange={(e)=>setLastName(e.target.value)} placeholder="Enter Last Name" id="l-input" className='input-filed' variant="filled" required />
                                </Grid>
                                    
                                <Grid item xs={12}>
                                    <label id='email' className='entry'>Email</label><br />
                                    <TextField style={{width:'95%'}} onChange={(e)=>setEmail(e.target.value)} placeholder="Enter Email id" id="e-input" className='input-filed' variant="filled" required />
                                </Grid>
                                        
                                <Grid item xs={12}>
                                    <label id='dob' className='entry'>DOB</label><br />
                                    <LocalizationProvider id='d-input' dateAdapter={AdapterDateFns} required  >
                                        <DatePicker 
                                            placeholder="DD/MM/YYYY"
                                            value={Date}
                                            onChange={(newValue) => {
                                                setDate(newValue);
                                            }}
                                            renderInput={(params) => <TextField {...params} />}
                                        />
                                    </LocalizationProvider>
                                </Grid>

                                <Grid item xs={12}>
                                    <label id='gender' className='entry'>Gender</label><br />
                                    <FormControl component="fieldset">
                                        <RadioGroup row aria-label="gender" name="row-radio-buttons-group" value={Gender} onChange={(e)=>setGender(e.target.value)} required >
                                            <FormControlLabel id='male' value="male"  control={<Radio />} label="Male" />
                                            <FormControlLabel id='female' value="female" control={<Radio />} label="Female" />
                                        </RadioGroup>
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12}>
                                    <Button id="button" variant="contained">SUBMIT</Button>
                                </Grid>
                                
                            </Grid>
                        </FormControl>
                    </Box>
                    
                    
                </Grid>
            </Grid>
        </div>
    )
}

export default Signup
