import './App.css';
import React from 'react';
import Phone from './Phone';
import { Link } from 'react-router-dom';


class Login extends React.Component{
  render(){
  return (
  <div className="App">
    <div class="container">
      <div class="row gx-0">
      <div class="col-md-6">
      <div class="leftside d-flex justify-content-center align-items-center ">
    <div className="conti-img">
    <img src="images/login.png" class="img" alt="loginimg" ></img>
    </div>

    </div>
   
    
    </div>
      <div class="col-md-6">
        <div class="rightside d-flex justify-content-center align-items-center">
          <div className="wrap-login100  p-l-55 p-r-55 p-t-65 p-b-50">
            <form  className="login100-form" >
                <span className="login100-form-title  p-b-33 a">
                    Bodh Labs<br></br>
                </span>
                
                <span className="login1000-form-title d-flex justify-content-center align-items-center p-b-33">
                    SIGN IN TO YOUR ACCOUNT
                </span>
                <div>
                <label for="validationTextarea" class="diss">Mobile Number</label>
                <Phone/>
                </div>
                
                            
                <div className="container-login100-form-btn m-t-20">
                    <button className="login100-form-btn" type="submit" >
                       <Link to='/otp'>Get Otp</Link>
                    </button>
                </div>

                <div className="text-center p-t-45 p-b-4">
                    <span className="txt1">
                        Any Query ?
                    </span>

                    <a href="#" >
                          <t> Help & Support</t>
                    </a>
                </div>

            </form>
        </div>

        </div>
    
      </div>
  </div>
</div>
</div>
  );
}
}

export default Login;
