import React from 'react';
  
class Validation extends React.Component {
    constructor() {
    super();
    this.state = {
      input: {},
      errors: {}
    };
     
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
    
  handleChange(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;
  
    this.setState({
      input
    });
  }
     
  handleSubmit(event) {
    event.preventDefault();
  
    if(this.validate()){
        console.log(this.state);
  
        let input = {};
        input["PhoneInputInput"] = "";
        this.setState({input:input});
        alert('Demo Form is submited');
    }
  }
  validate(){
      let input = this.state.input;
      let errors = {};
      let isValid = true;
  
  
      if (!input["PhoneInputInput"]) {
        isValid = false;
        errors["PhoneInputInput"] = "Please enter your phone number.";
      }
  
      if (typeof input["PhoneInputInput"] !== "undefined") {
          
        var pattern = new RegExp(/^[0-9\b]+$/);
        if (!pattern.test(input["PhoneInputInput"])) {
          isValid = false;
          errors["PhoneInputInput"] = "Please enter only number.";
        }else if(input["PhoneInputInput"].length != 10){
          isValid = false;
          errors["PhoneInputInput"] = "Please enter valid phone number.";
        }
      }
  
  
      this.setState({
        errors: errors
      });
  
      return isValid;
  }
     
  render() {
    return (
      <div>
        <h1>React Validation For Phone Number Example - ItSolutionStuff.com</h1>
        <form onSubmit={this.handleSubmit}>
  
            
          <div class="form-group">
            <label for="Phone">Phone:</label>
            <input 
              type="text" 
              name="phone" 
              value={this.state.input.phone}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter phone" 
              id="email" />
  
              <div className="text-danger">{this.state.errors.phone}</div>
          </div>
  
              
          <input type="submit" value="Submit" class="btn btn-success" />
        </form>
      </div>
    );
  }
}
  
export default Validation;