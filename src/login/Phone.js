import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isValidPhoneNumber } from 'react-phone-number-input'
import './Phone.css';
import React, { useState } from 'react';

const Phone = () => {
    const [value, setValue] = useState()
     
    return (
      <div>
      <PhoneInput
        className="phone"
        placeholder="Enter Phone Number"
        maxLength={setValue=="+91"  ?  14 : 15}
        countryCallingCodeEditable={true}
        initialValueFormat="national"
        // defaultCountry="IN"
        value={value}
        onChange={setValue}
        required
        error={ value ? (isValidPhoneNumber(value) ? undefined : 'Invalid phone number') : 'Phone number required'}
        international
        />
        
{/* 
      Is valid: {value && isValidPhoneNumber(value) ? 'true' : 'false'}
      National: {value && formatPhoneNumber(value)}
      International: {value && formatPhoneNumberIntl(value)} */}
        </div>
    ); 
    
};

export default Phone;
