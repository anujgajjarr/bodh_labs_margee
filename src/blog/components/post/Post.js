import { Link } from "react-router-dom";
import "./post.css";

export default function Post({img}) {
  return (
    
    

    <div className="post">
    
      <img
        className="postImg"
        src={img}
        alt="Blogs"
      />
      <div className="postInfo">
        {/* <div className="postCats">
          <span className="postCat">
            <Link className="link" to="/posts?cat=Music">
              Music
            </Link>
          </span>
          <span className="postCat">
            <Link className="link" to="/posts?cat=Music">
              Life
            </Link>
          </span>
        </div> */}
        <span className="postTitle">
          <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link>
        </span>
        {/* <hr /> */}
        <span className="postDate">16 December, 2021</span>
      </div>
      
     
    </div>
  );
}
