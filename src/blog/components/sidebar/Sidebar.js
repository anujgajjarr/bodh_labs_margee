import { Link } from "react-router-dom";
import "./sidebar.css";

export default function Sidebar() {
  return (
    <div className="sidebar">
    <div class="vl"></div>
      <div className="sidebarItem">
        <span className="sidebarTitle">Recommended Blogs</span>
        
        <div class="row">
        <div class="left-side col-md-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-md-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>
    <br></br>


      <div class="row">
        <div class="left-side col-md-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-md-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>

      <br></br>

      <div class="row">
        <div class="left-side col-md-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-md-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>
    <br></br>
        
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">International Blogs</span>
        
        <div class="row">
        <div class="left-side col-md-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-md-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>
    <br></br>


      <div class="row">
        <div class="left-side col-sm-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-sm-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>

      <br></br>

      <div class="row">
        <div class="left-side col-sm-6 no-gutters">
        <img
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt="imp blog"
        />
       </div>
        <div class="right-side col-sm-6 no-gutters">
        <span className="sidebarTit"> <Link to="/post/abc" className="link">
          Fasninated Unheard Story of IAF’s The Knights Squadron
          </Link></span>
          <br></br>
          <span className="postDate">16 December, 2021</span>
        </div>
      </div>
    <br></br>
    </div>
     
      </div>
    
  );
}
