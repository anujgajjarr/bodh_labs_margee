import React from "react";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import "./topbar.css";
import "./topbarButton.js";


export default function Topbar() {
  const user = true;
  return (
    <div className="top1">
    <div className="container">
      <div className="row row-cols-2">


    <div className="h11 col-md-8 no-gutters">
    <div className='heading'><NavLink className="link " exact to="/" activeClassName="nav-active" >
                 Bodh Labs
            </NavLink></div>
    </div>

    <div className="h11 col-6 col-md-4 no-gutters">
    {/* <div className='heading1'>
    <div className="topRight1 ">
        <i className="topIcon fab fa-facebook-square"></i>
        <i className="topIcon fab fa-instagram-square"></i>
        <i className="topIcon fab fa-twitter-square"></i>
        </div>
    </div> */}
    </div>
  </div>
  {/* <div className="row">
    <div className="col-md-8">col-sm-8</div>
    <div className="col-md-4">col-sm-4</div>
  </div> */}
</div>
    {/* <div class="row row-no-gutters">
  <div class="col-xs-12 col-md-8 row-no-gutters">
  <div className="left">Bodh labs</div>
  </div>

  <div class="col-xs-6 col-md-4 row-no-gutters">
  <div className="topRight">
        <i className="topIcon fab fa-facebook-square"></i>
        <i className="topIcon fab fa-instagram-square"></i>
        <i className="topIcon fab fa-pinterest-square"></i>
        <i className="topIcon fab fa-twitter-square"></i>
      </div> </div>
</div> */}

    {/* <nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <link class="navbar-brand" href="#">Navbar</link>
  </div>
</nav> */}
      {/* */}
      
      <div className="menu" id="toggle-Button">
        <div className="menu-line"></div>
        <div className="menu-line"></div>
        <div className="menu-line"></div>
      </div>

      <div className='heading1'><NavLink className="link " exact to="/" activeClassName="nav-active" >
                 Bodh Labs
            </NavLink></div>
      {/* <div className="topRight col-6 col-md-4" >
      <i className="topSearchIcon fas fa-search" ></i>
        {user ? (
          
          <NavLink className="link"  to="/settings">
            <img
              className="topImg"
              src="https://images.pexels.com/photos/1858175/pexels-photo-1858175.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
              alt=""
            />
          </NavLink>
        ) : (
          <ul className="topList">
            <li className="topListItem">
              <NavLink className="link" >
                LOGIN
              </NavLink>
            </li>
            <li className="topListItem">
              <NavLink className="link" >
                REGISTER
              </NavLink>
            </li>
          </ul>
        )}
      </div> */}




      <div className="containerr">
      <div className="row row-cols-2">
      <div className="topLeft col-md-8 no-gutters" id="navi-list">
        <ul className="topList">
          <li className="topListItem " >
          <NavLink className="link " exact to="/" activeClassName="nav-active" >
              Home
            </NavLink>
            </li>
          <li className="topListItem">
            <NavLink className="link " exact to="/Health" activeClassName="nav-active" >
              Health
            </NavLink>
          </li>
          <li className="topListItem">
            <NavLink className="link " exact to="/sports" activeClassName="nav-active" >
              Sports
            </NavLink>
          </li>
          <li className="topListItem">
            <NavLink className="link" exact to="/Politics" activeClassName="nav-active">
              Politics
            </NavLink>
          </li>
          {user && <li className="topListItem"><NavLink className="link " exact to="/Bollywood" activeClassName="nav-active" >
              Bollywood
            </NavLink></li>}
            <div className="phoneNavbarIcons ">
            <i className="col-md-6 topSearchIcon fas fa-search"></i>
            
          
          <Link className="col-md-6 link" to="./Bollywood">
            <img
              className="topImg"
              src="https://images.pexels.com/photos/1858175/pexels-photo-1858175.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
              alt=""
            />
          </Link>
        </div>
        </ul>
      </div>
      



      <div className="topRight col-6 col-md-4" >
      <i className="topSearchIcon fas fa-search" ></i>
        {user ? (
          
          <NavLink className="link" to="/Bollywood">
            <img
              className="topImg"
              src="https://images.pexels.com/photos/1858175/pexels-photo-1858175.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
              alt=""
            />
          </NavLink>
        ) : (
          <ul className="topList">
            <li className="topListItem">
              <NavLink className="link" >
                LOGIN
              </NavLink>
            </li>
            <li className="topListItem">
              <NavLink className="link" >
                REGISTER
              </NavLink>
            </li>
          </ul>
        )}
      </div>


    </div>
    </div>
    </div>
  );
}
