import Topbar from "./components/topbar/Topbar";
import Homepage from "./pages/homepage/Homepage";
import Bollywood from "./pages/settings/Bollywood";
import Single from "./pages/single/Single";
import Politics from "./pages/write/Politics";
import Health from "./pages/Health/Health";
import Sports from "./pages/sports/Sports";


import { BrowserRouter as Router,Switch,Route} from "react-router-dom";

function App3() {
  const currentUser = true;
  return (
    <Router>
    <Topbar />
    <Switch>
      <Route exact path="/">
        <Homepage />
      </Route>
      <Route path="/Health">{currentUser ? <Health /> : <Homepage />}</Route>
      <Route path="/sports">{currentUser ? <Sports /> : <Homepage />}</Route>
      <Route path="/post/:id">
        <Single />
      </Route>
      <Route path="/Politics">{currentUser ? <Politics /> : <Homepage />}</Route>
      <Route path="/Bollywood">
        {currentUser ? <Bollywood /> : <Homepage />}
      </Route>
    </Switch>
  </Router>
  );
}

export default App3;
